# Notes Recognition

A neural network that helps you to recognise (transcribe) notes in recorded piano compositions.

This program is distributed without the hope that it will be useful but for fun. The problem of music transcription is the
most interesting one (I suggest) at the intersection of music and machine learning.

## Background
Theano + Lasagne
Librosa and Scipy for wav processing

## Architecture
Was almost copied from this [article](https://arxiv.org/pdf/1409.7495.pdf). There is a CNN that tries to recognize notes in
generated (MIDI) music and real music (the last one is much more difficult) and a discriminator, which is supposed to make
CNN not to tell MIDI sound from real one. CNN is trained only on generated files, while discriminator is, of course, trained
on both.

## Dataset
It was the hardest. I was quite disappointed with the fact that there is almost no labeled datasets of real music chords. And
if it is, they are unreachable, very small, awfully recorded, etc.
So I found two with labeled real piano single notes and then made chords out of them, summing all data in selected files.
It was much easier to make clear (artificial, unreal) dataset for CNN training. There was just midi files generated and processed to
wav format.

### Many thanks to
[Goto project school](https://goto.msk.ru/)

[V. Lempitsky's cool article](https://arxiv.org/pdf/1409.7495.pdf)

The ones who are still with me despite all my laziness and stubbornness.




# Распознавание нот в фортепьянных произведениях

Нейросеть, позволяющая распознавать ноты в записанных фортепьянных композициях.
Этот проект создан в меньшей степени для того, чтобы принести кому-то пользу, и в большей - чтобы позаниматься интересными исследованиями на стыке музыки и машинного обучения.

## Используемые библиотеки
Theano + Lasagne

Librosa и Scipy для обработки wav файлов

## Архитектура нейросети
Основана на статье В. Лемпицкого и Я. Ганина [“Unsupervised Domain Adaptation by Backpropagation”](https://arxiv.org/pdf/1409.7495.pdf). 
Есть сверточная нейронная сеть, которая пытается распознать ноты в сгенерированных (без помех и шумов и не очень похожих на звуки реального фортепьяно; 
распознавать ноты в таких записях легко) звуковых файлах и в реальных произведениях (что является основной сложностью и конечной целью проекта). 
Так же есть дискриминатор, с помощью которого мы пытаемся дать сверточной сеть понять, что реальные произведения на самом деле мало отличаются от сгенерированных.
Свертка учится только на сгенерированных файлах, тогда как дискриминатор - и на сгенерированных, и на реальных.

## Датасеты
Самая сложная часть проекта. К сожалению, в интернете очень мало действительно подходящих под задачу датасетов с реальными данными, а те, что есть, 
либо недосягаемы (например, с техподдержкой одной очень хорошей большой базы с размеченными данными пришлось переписываться неделю, прежде чем узнать, 
что у них накрылся сервер и 31 ГБ прекрасных данных канул в Лету), либо очень маленькие, либо очень плохо записаны или не размечены и так далее. 
Тем не менее, мне удалось найти один небольшой подходящий датасет.
Гораздо проще было сгенерировать данные для обучения свертки - благо, формат MIDI легко поддается программной генерации и последующей конвертации в формат wav.

## Благодарности
[Проектной школе Goto](https://goto.msk.ru/) за помощь и предоставленного куратора

В. Лемпицкому и Я. Ганину за полезную статью

Всем тем, кто до сих пор меня поддерживает несмотря на мою лень и упертость :)
